# Project-WMusic

CONTENTS OF THIS FILE
---------------------

* Introduction
* How to RUN
* Project URL


INTRODUCTION
------------

The basis of this project is to use Recurrent Neural Networks and a stream of midi files to generate new music. This project hopes to determine whether - an AI could create substantial compositions with musicality indistinguishable from pieces written by people. It does not need to replicate the exact musical form of the midi, simply replicate the patterns exhibited within them. Through this, the hopes are that through a simple press of a button, we can generate a new musical composition.

HOW TO RUN
--------------
Run the JAVA file using any JAVA compiling software. 



Project URL: https://mwatts559974197.wordpress.com/

mwatt004
